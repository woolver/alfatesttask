package com.AlfaTask;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        // write your code here
        //
        try {


            BufferedReader reader =
                    new BufferedReader(
                            new InputStreamReader(
                                    new FileInputStream("src//com//AlfaTask//input.txt"), StandardCharsets.UTF_8));
            List<Integer> intlist = new ArrayList<Integer>();
            reader.lines().forEach(s -> intlist.addAll(parsStringToList(s)));
            reader.close();
            Collections.sort(intlist);
            System.out.println("ASC sort:");
            printList(intlist);
            System.out.println("DESC sort:");
            Collections.reverse(intlist);
            printList(intlist);
        }
        catch (IOException ex)
        {
            System.out.println("Error is " + ex.toString());
        }

        System.out.println("Задача факториала 20!");
        System.out.print(factorial(20));
    }


    public static List<Integer> parsStringToList (String strforparce){

        List <Integer> stringList = new ArrayList<Integer>();
        String[] tmpFileString = strforparce.split(",");
        for(int i = 0; tmpFileString.length> i; i++)
            stringList.add(Integer.parseInt(tmpFileString[i]));
        return stringList;
    }

    public static void printList  (List<Integer> listForPrint){
        for(int i = 0; i < listForPrint.size(); i++)
            System.out.println(listForPrint.get(i));
    }

    public static long factorial(Integer fact){
        long result=1;
        if(fact>0)
            for(int i = 2; i <= fact; i++)
                result*=i;
        else
            return -1;
        return result;
    }
}
